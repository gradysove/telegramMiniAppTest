angular.module('weatherApp', [])
.controller('WeatherController', function($scope, $http) {
    $scope.formatTemperature = function(temp) {
        return (temp > 0 ? '+' : '') + Math.round(temp) + '°C';
    };

    $scope.getWeather = function() {
        const apiKey = '40e49f62dcddcfbbaaa5aadbfe3a897c'; // Замените на ваш API ключ OpenWeatherMap
        const city = $scope.city.split(',')[0]; // Извлекаем название города
        const weatherApiUrl = `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&lang=ru&appid=${apiKey}`;

        // Получение текущей погоды
        $http.get(weatherApiUrl)
        .then(function(response) {
            $scope.weather = response.data;
            const lat = response.data.coord.lat;
            const lon = response.data.coord.lon;

            // Получение прогноза погоды на следующие сутки с интервалом в три часа
            const forecastApiUrl = `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&units=metric&lang=ru&appid=${apiKey}`;
            return $http.get(forecastApiUrl);
        })
        .then(function(response) {
            // Фильтруем данные, чтобы получить прогноз только на следующие сутки
            const now = new Date().getTime() / 1000;
            const oneDayLater = now + 24 * 60 * 60;
            $scope.forecast = response.data.list.filter(item => item.dt <= oneDayLater);
        })
        .catch(function(error) {
            console.error('Ошибка при получении данных:', error);
            alert('Не удалось получить данные о погоде. Проверьте правильность ввода города.');
        });
    };
});
